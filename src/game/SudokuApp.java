/*
 * Matt Fischer
 * CS 5551
 * Final Project
 * 11/23/10
 */

package game;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.WindowConstants;

//Driver class setting up all the different components and handles actions from
//the user.
public class SudokuApp implements ActionListener, Printable {

    private JFrame frame;
    private JMenuBar menuBar;
    private GameGrid grid;
    private SudokuGUI panel;
    private String difficulty = "Easy";

    //Main method; creates and shows the gui
    public static void main(String[] args) {
        SudokuApp app = new SudokuApp();
        app.createAndShowGUI();
    }

    //Returns the game grid holding all puzzle data
    public GameGrid getGameGrid() {
        return grid;
    }

    //Returns the gui
    public SudokuGUI getGUI() {
        return panel;
    }

    //Sets up the gui
    private void createAndShowGUI() {
        frame = new JFrame("Sudoku");
        menuBar = createMenuBar();
        grid = new GameGrid(difficulty, this);
        panel = new SudokuGUI(this);

        frame.setJMenuBar(menuBar);
        frame.add(panel);

        frame.pack();
        frame.setMinimumSize(new Dimension(300, 300));
        frame.setPreferredSize(new Dimension(600, 600));
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

    //Creates the menubar
    private JMenuBar createMenuBar() {
        JMenu menu;
        JMenuItem menuItem;
        JRadioButtonMenuItem rbMenuItem;
        ButtonGroup group = new ButtonGroup();

        JMenuBar tempMenuBar = new JMenuBar();


        //Creates the "file" menu item
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);

        menuItem = new JMenuItem("New");
        menuItem.setMnemonic(KeyEvent.VK_N);
        menuItem.setActionCommand("New");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menuItem = new JMenuItem("Clear");
        menuItem.setMnemonic(KeyEvent.VK_C);
        menuItem.setActionCommand("Clear");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menu.addSeparator();

        menuItem = new JMenuItem("Print");
        menuItem.setMnemonic(KeyEvent.VK_P);
        menuItem.setActionCommand("Print");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        tempMenuBar.add(menu);
        tempMenuBar.add(Box.createHorizontalStrut(10));

        //Creates the difficulty menu item
        menu = new JMenu("Difficulty");
        menu.setMnemonic(KeyEvent.VK_D);

        rbMenuItem = new JRadioButtonMenuItem("Easy");
        rbMenuItem.setMnemonic(KeyEvent.VK_E);
        rbMenuItem.setActionCommand("Easy");
        rbMenuItem.setSelected(true);
        rbMenuItem.addActionListener(this);
        group.add(rbMenuItem);
        menu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("Medium");
        rbMenuItem.setMnemonic(KeyEvent.VK_M);
        rbMenuItem.setActionCommand("Medium");
        rbMenuItem.addActionListener(this);
        group.add(rbMenuItem);
        menu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("Hard");
        rbMenuItem.setMnemonic(KeyEvent.VK_H);
        rbMenuItem.setActionCommand("Hard");
        rbMenuItem.addActionListener(this);
        group.add(rbMenuItem);
        menu.add(rbMenuItem);

        tempMenuBar.add(menu);
        tempMenuBar.add(Box.createHorizontalStrut(10));

        JButton b = new JButton("Solve");
        b.setActionCommand("Solve");
        b.addActionListener(this);

        tempMenuBar.add(b);
        tempMenuBar.add(Box.createHorizontalGlue());

        //Creates the help menu item
        menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_H);

        menuItem = new JMenuItem("How to play");
        menuItem.setMnemonic(KeyEvent.VK_H);
        menuItem.setActionCommand("How to play");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menuItem = new JMenuItem("About");
        menuItem.setMnemonic(KeyEvent.VK_A);
        menuItem.setActionCommand("About");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        tempMenuBar.add(menu);

        return tempMenuBar;
    }

    //Takes in an action event and calls cycle button on the specified one
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        int rowPos, colPos, rowCol, newVal;
        JButton button;

        if (cmd.equalsIgnoreCase("New")) {
            grid.setGrid(difficulty);
            panel.reInitGrid();
        }
        else if (cmd.equalsIgnoreCase("Clear")) {
            grid.clearGrid();
            panel.reInitGrid();
        }
        else if (cmd.equalsIgnoreCase("Print")) {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                }
                catch (PrinterException ex) {
                    System.err.println(ex.getLocalizedMessage());
                }
            }
        }
        else if (cmd.equalsIgnoreCase("Solve")) {
            if (grid.hasWon()) {
                new Popups("Solved");
            }
            else {
                new Popups("Not Solved");
            }
        }
        else if (cmd.equalsIgnoreCase("Easy") || cmd.equalsIgnoreCase("Medium")
                || cmd.equalsIgnoreCase("Hard")) {
            difficulty = cmd;
            grid.setGrid(difficulty);
            panel.reInitGrid();
        }
        else if (cmd.equalsIgnoreCase("How to play")
                || cmd.equalsIgnoreCase("About")) {
            new Popups(cmd);
        }
        else {
            rowCol = Integer.parseInt(cmd);
            rowPos = rowCol / 10;
            colPos = rowCol % 10;
            button = (JButton) panel.getComponent(rowPos * 9 + colPos);
            newVal = cycleButton(button);
            grid.setCell(newVal, rowPos, colPos);
            panel.resetColors();
        }
    }

    //cycles the buttons "blank" through 9 then back to "blank"
    private int cycleButton(JButton button) {

        if (button.getText().equalsIgnoreCase("")) {
            button.setText("1");
            return 1;
        }
        else if (button.getText().equalsIgnoreCase("1")) {
            button.setText("2");
            return 2;
        }
        else if (button.getText().equalsIgnoreCase("2")) {
            button.setText("3");
            return 3;
        }
        else if (button.getText().equalsIgnoreCase("3")) {
            button.setText("4");
            return 4;
        }
        else if (button.getText().equalsIgnoreCase("4")) {
            button.setText("5");
            return 5;
        }
        else if (button.getText().equalsIgnoreCase("5")) {
            button.setText("6");
            return 6;
        }
        else if (button.getText().equalsIgnoreCase("6")) {
            button.setText("7");
            return 7;
        }
        else if (button.getText().equalsIgnoreCase("7")) {
            button.setText("8");
            return 8;
        }
        else if (button.getText().equalsIgnoreCase("8")) {
            button.setText("9");
            return 9;
        }
        else if (button.getText().equalsIgnoreCase("9")) {
            button.setText("");
            return 0;
        }
        else {
            System.err.println("Error: Cycle Button");
            return -1;
        }
    }

    //Prints an easy to use puzzle for hand solving
    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
        final int SIZE = 300;
        int space = Math.round(SIZE / 9);
        int widthHeight = 9 * space;
        int xCushion = Math.round(space / 2) - 4;
        int yCushion = Math.round(space / 2) + 4;
        int[][] tempGrid = grid.getGrid();

        if (page > 0) {
            return NO_SUCH_PAGE;
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());

        for (int i = 0; i < 10; i++) {
            int pos = i * space;

            g2d.drawLine(pos, 0, pos, widthHeight);
            g2d.drawLine(0, pos, widthHeight, pos);

            if (i == 3 || i == 6) {
                g2d.drawLine(pos - 1, 0, pos - 1, widthHeight);
                g2d.drawLine(pos + 1, 0, pos + 1, widthHeight);

                g2d.drawLine(0, pos - 1, widthHeight, pos - 1);
                g2d.drawLine(0, pos + 1, widthHeight, pos + 1);
            }

        }

        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                int x = n * space + xCushion;
                int y = m * space + yCushion;

                if (tempGrid[n][m] == 0) {
                    g2d.drawString("", x, y);
                }
                else {
                    g2d.drawString(Integer.toString(tempGrid[n][m]), x, y);
                }
            }
        }

        return PAGE_EXISTS;

    }
}
