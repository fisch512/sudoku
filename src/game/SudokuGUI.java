/*
 * Matt Fischer
 * CS 5551
 * Final Project
 * 11/23/10
 */
package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

//Represents the GUI of the puzzle
public class SudokuGUI extends JPanel {

    private JButton button;
    private SudokuApp app;
    private GameGrid grid;

    //Constructor
    public SudokuGUI(SudokuApp app) {
        super();
        this.app = app;
        grid = app.getGameGrid();
        this.setPreferredSize(new Dimension(600, 600));
        this.setLayout(new GridLayout(9, 9));
        this.initGrid();
    }

    //Highlights a single empty cell red
    public void showEmptyError(int row, int column) {
        button = (JButton)this.getComponent(row * 9 + column);
        button.setBackground(Color.red);
    }

    //Highlights an entire row red
    public void showRowError(int row) {
        for(int i = 0; i < 9; i++) {
            button = (JButton)this.getComponent(row * 9 + i);
            button.setBackground(Color.red);
        }
    }

    //Highlights an entire column red
    public void showColumnError(int column) {
        for(int i = 0; i < 9; i++) {
            button = (JButton)this.getComponent(i * 9 + column);
            button.setBackground(Color.red);
        }
    }

    //Highlights a mini 3x3 grid
    public void showMiniGridError(int x, int y) {
        for(int i = x * 3; i < ((x + 1) * 3); i++) {
            for(int j = y * 3; j < ((y + 1) * 3); j++) {
                button = (JButton) this.getComponent(i * 9 + j);
                button.setBackground(Color.red);
            }
        }
    }

    //Resets the gui grid
    public void reInitGrid() {
        JButton b;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                b = (JButton) this.getComponent(i * 9 + j);
                if(grid.getCell(i, j) == 0) {
                    b.setText("");
                    b.setEnabled(true);
                } else {
                    b.setText(String.valueOf(grid.getCell(i, j)));
                    b.setEnabled(false);
                }
            }
        }
    }

    //Sets up the gui grid
    private void initGrid() {

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if(grid.getCell(i, j) == 0) {
                    button = new JButton();
                    button.setEnabled(true);
                } else {
                    button = new JButton(String.valueOf(grid.getCell(i, j)));
                    button.setEnabled(false);
                }

                button.setFont(new Font("Sans-serif", Font.BOLD, 18));
                button.setActionCommand("" + i + j);
                button.addActionListener(app);
                button.setBackground(getColor(i, j));
                this.add(button);
            }
        }
    }

    //Returns the color of a specific cell
    public Color getColor(int row, int col) {
        if ((row < 3 || row > 5) && (col < 3 || col > 5)) {
            return Constants.DARKBLUE;
        }
        else if (col > 2 && col < 6 && row > 2 && row < 6) {
            return Constants.DARKBLUE;
        }
        else {
            return Constants.LIGHTBLUE;
        }
    }

    //Removes any red highlighting
    void resetColors() {
        JButton b;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                b = (JButton) this.getComponent(i * 9 + j);
                b.setBackground(getColor(i, j));
            }
        }
    }
}
