/*
 * Matt Fischer
 * CS 5551
 * Final Project
 * 11/23/10
 */
package game;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

//Used to display pop up messages to the user
public class Popups extends JFrame {

    public Popups(String str) {
        super();

        this.setTitle(str);
        this.setMaximumSize(new Dimension(300, 200));
        this.setResizable(false);
        this.setLocation(400, 100);
        this.setBackground(Constants.LIGHTBLUE);

        this.add(createText(str));

        this.pack();
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    private JTextArea createText(String str) {

        JTextArea text = new JTextArea();
        text.setFont(new Font("Sans-serif", Font.PLAIN, 18));
        text.setMaximumSize(new Dimension(300, 200));
        text.setBackground(Constants.LIGHTBLUE);


        if (str.equalsIgnoreCase("How to play")) {
            text.setText(Constants.HOW_TO_PLAY);
        }
        else if (str.equalsIgnoreCase("About")) {
            text.setText(Constants.ABOUT);
        }
        else if(str.equalsIgnoreCase("Solved")) {
            text.setText(Constants.SOLVED);
        }
        else if(str.equalsIgnoreCase("Not Solved")) {
            text.setText(Constants.NOT_SOLVED);
        }
        else {
            text.setText("Error");
        }

        return text;
    }
}
