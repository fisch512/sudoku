/*
 * Matt Fischer
 * CS 5551
 * Final Project
 * 11/23/10
 */
package game;

import java.util.Arrays;
import java.util.Random;

//Holds and minipulates the data to represent a puzzle
public class GameGrid {

    private int[][] grid;
    private int[][] selectedGrid;
    private int[] column;
    private int[] row;
    private int[][] miniGrid;
    //Set of the valid numbers
    private static int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
    private SudokuGUI gui;
    private SudokuApp app;

    //Constructor
    public GameGrid() {
        grid = new int[9][9];
    }

    //Constructor
    public GameGrid(String str, SudokuApp app) {
        if(str.equalsIgnoreCase("Easy")) {
            grid = transformGrid(copyGrid(Puzzles.EASY));
        } else if(str.equalsIgnoreCase("Medium")) {
            grid = transformGrid(copyGrid(Puzzles.MEDIUM));
        } else if(str.equalsIgnoreCase("Hard")) {
            grid = transformGrid(copyGrid(Puzzles.HARD));
        } else {
            grid = transformGrid(copyGrid(Puzzles.EASY));
        }

        this.app = app;
        selectedGrid = copyGrid(grid);
    }

    //sets the current game grid based off a selected difficulty
    public void setGrid(String str) {
        if(str.equalsIgnoreCase("Easy")) {
            grid = transformGrid(copyGrid(Puzzles.EASY));
        } else if(str.equalsIgnoreCase("Medium")) {
            grid = transformGrid(copyGrid(Puzzles.MEDIUM));
        } else if(str.equalsIgnoreCase("Hard")) {
            grid = transformGrid(copyGrid(Puzzles.HARD));
        } else {
            grid = transformGrid(copyGrid(Puzzles.EASY));
        }

        selectedGrid = copyGrid(grid);

    }

    //sets the current game grid with another game grid
    public void setGrid(int[][] g) {
        grid = g;
        selectedGrid = g;
    }

    //clears out all user set cells
    public void clearGrid() {
        grid = copyGrid(selectedGrid);
    }

    //returns the puzzle data
    public int[][] getGrid() {
        return copyGrid(grid);
    }

    //2d array copy
    private int[][] copyGrid(int[][] a) {
        int[][] tempGrid = new int[9][9];
        for(int i = 0; i < 9; i++) {
            for(int j = 0; j < 9; j++) {
                tempGrid[i][j] = a[i][j];
            }
        }
        return tempGrid;
    }

    //Performs swaps to make a unique puzzle
    private int[][] transformGrid(int[][] tempGrid) {
        int cycles = (int)(Math.random() * 10 + 2);

        for(int i = 0; i < cycles; i++) {
            if(randomTrueOrFalse()) {
                swapRow(tempGrid);
            }
            if(randomTrueOrFalse()) {
                swapColumn(tempGrid);
            }
            if(randomTrueOrFalse()) {
                swapMajorDiagonal(tempGrid);
            }
            if(randomTrueOrFalse()) {
                swapMinorDiagonal(tempGrid);
            }
        }
        return tempGrid;
    }

    //Swaps the cells across the middle of the columns
    private void swapRow(int[][] tempGrid) {
        int temp;
        for(int i = 0; i < 9; i++) {
            for(int j = 0, k = 8; j < 4; j++, k--) {
                temp = tempGrid[i][j];
                tempGrid[i][j] = tempGrid[i][k];
                tempGrid[i][k] = temp;
            }
        }
    }

    //Swaps the cells across the middle of the columns
    private void swapColumn(int[][] tempGrid) {
        int temp;
        for(int i = 0; i < 9; i++) {
            for(int j = 0, k = 8; j < 4; j++, k--) {
                temp = tempGrid[j][i];
                tempGrid[j][i] = tempGrid[k][i];
                tempGrid[k][i] = temp;
            }
        }
    }

    //Swaps the cells across the major diagonal
    private void swapMajorDiagonal(int[][] tempGrid) {
        int temp;
        for(int j = 0; j < 9; j++) {
            for(int i = 0; i < 9; i++) {
                if(i < (8 - j)) {
                    temp = tempGrid[i][j];
                    tempGrid[i][j] = tempGrid[8 - j][8 - i];
                    tempGrid[8 - j][8 - i] = temp;
                }
            }
        }
    }

    //Swaps the cells across the minor diagonal
    private void swapMinorDiagonal(int[][] tempGrid) {
        int temp;
        for(int j = 0; j < 9; j++) {
            for(int i = 0; i < 9; i++) {
                if(i > j) {
                    temp = tempGrid[i][j];
                    tempGrid[i][j] = tempGrid[j][i];
                    tempGrid[j][i] = temp;
                }
            }
        }
    }
    //Returns the value of a specific cell
    public int getCell(int row, int col) {
        if (row < 0 || row > 8 || col < 0 || col > 8) {
            return 0;
        }
        else {
            return grid[row][col];
        }
    }

    //Sets the value of a specific cell
    public void setCell(int value, int row, int col) {
        if(value == -1) {
            System.err.println("SetEntry out of bounds: " + value);
        }
        else if(value < 0 || value > 9 || row < 0 || row > 8 || col < 0 || col > 8) {
            System.err.println("SetEntry out of bounds: " + value);
        }
        else {
            grid[row][col] = value;
        }
    }

    //Returns true if the player has won, false otherwise.
    public boolean hasWon() {
        this.gui = app.getGUI();
        //Check for empty cells
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (grid[i][j] == 0) {
                    gui.showEmptyError(i, j);
                    return false;
                }
            }
        }

        //Check each row
        for (int i = 0; i < 9; i++) {
            row = this.createRow(i);
            if (!this.numberCheck(row)) {
                gui.showRowError(i);
                return false;
            }
        }

        //Check each column
        for (int i = 0; i < 9; i++) {
            column = this.createColumn(i);
            if (!this.numberCheck(column)) {
                gui.showColumnError(i);
                return false;
            }
        }

        //Check each miniGrid
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                miniGrid = this.createMiniGrid(i, j);
                if (!this.numberCheck(miniGrid)) {
                    gui.showMiniGridError(i, j);
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public String toString() {
        String str = "";

        for(int i = 0; i < 9; i++) {
            for(int j = 0; j < 9; j++) {
                str += grid[i][j];
            }
            str += "\n";
        }

        return str;
    }

    //Checks for the numbers 1 - 9 in a 1d array
    private boolean numberCheck(int[] array) {
        int[] copy = Arrays.copyOf(array, array.length);
        Arrays.sort(copy);

        return Arrays.equals(copy, numbers);
    }

    //Checks for the numbers 1 - 9 in a 2d array
    private boolean numberCheck(int[][] array) {
        int[] copy1d = new int[9];
        copy1d[0] = array[0][0];
        copy1d[1] = array[0][1];
        copy1d[2] = array[0][2];
        copy1d[3] = array[1][0];
        copy1d[4] = array[1][1];
        copy1d[5] = array[1][2];
        copy1d[6] = array[2][0];
        copy1d[7] = array[2][1];
        copy1d[8] = array[2][2];


        return numberCheck(copy1d);
    }

    //Creates an array to represent one of the 3 by 3
    //mini grids
    private int[][] createMiniGrid(int row, int col) {
        int[][] mini = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                mini[i][j] = grid[i + (row * 3)][j + (col * 3)];
            }
        }
        return mini;
    }

    //Creates an array representing one column
    private int[] createColumn(int col) {
        int[] temp = new int[9];
        for (int i = 0; i < 9; i++) {
            temp[i] = grid[i][col];
        }
        return temp;
    }

    //Creates an array representing one row
    private int[] createRow(int row) {
        int[] temp = new int[9];
        for (int i = 0; i < 9; i++) {
            temp[i] = grid[row][i];
        }
        return temp;
    }

    //Returns a random boolean
    private boolean randomTrueOrFalse() {
        if(Math.random() < 0.5) {
            return true;
        } else {
            return false;
        }
    }

}
