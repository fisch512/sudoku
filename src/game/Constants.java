/*
 * Matt Fischer
 * CS 5551
 * Final Project
 * 11/23/10
 */
package game;

import java.awt.Color;


//Holds contants used throughout the program
public class Constants {

    protected final static String HOW_TO_PLAY = "\n The objective of the game is to fill "
            + "all the blank squares in a game with \n"
            + " the correct numbers. There are three very simple constraints to follow\n"
            + " In a 9 by 9 square Sudoku game:\n\n"
            + " Every row of 9 numbers must include all digits 1 through 9 in any order\n"
            + " Every column of 9 numbers must include all digits 1 through 9 in any order\n"
            + " Every 3 by 3 subsection of the 9 by 9 square must include all digits 1 through 9\n ";
    protected final static String ABOUT = " Java GUI based Sudoku \n"
            + "  Written by Matt Fischer";
    protected final static String SOLVED = "\n Congratulations, you solved the puzzle! \n";
    protected final static String NOT_SOLVED = "\n Sorry, at least one cell is incorrect (Highlighted in red). \n"
            + " You can find the game rules in the help section. \n";
    protected final static Color LIGHTBLUE = new Color(178, 235, 245);
    protected final static Color DARKBLUE = new Color(134, 192, 247);
}
